package com.WebServer.Reprository;

import com.WebServer.dao.UserDao;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<UserDao, Integer> {

}
