package com.WebServer.Reprository;

import com.WebServer.dao.StudentDao;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

public interface StudentRepository extends CrudRepository<StudentDao, Integer> {

}
