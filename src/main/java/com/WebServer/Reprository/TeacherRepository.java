package com.WebServer.Reprository;

import com.WebServer.dao.TeacherDao;
import org.springframework.data.repository.CrudRepository;


public interface TeacherRepository extends CrudRepository<TeacherDao, Integer> {

}