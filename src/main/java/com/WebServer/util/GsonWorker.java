package com.WebServer.util;

import com.google.gson.Gson;
import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class GsonWorker {
    Gson gson = new Gson();

}
