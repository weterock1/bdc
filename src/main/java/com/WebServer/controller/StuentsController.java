package com.WebServer.controller;

import com.WebServer.Service.StudentService;
import com.WebServer.dao.StudentDao;
import com.WebServer.dto.StudentDto;
import com.google.gson.Gson;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Data
@Controller
@RequestMapping(value = "/student")
public class StuentsController {

    @Autowired
    StudentService studentService;

    @Autowired
    Gson gson;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String upstudent() {

        return "students";
    }

    @RequestMapping(value = "/save", method = RequestMethod.GET)
    public String save(@RequestParam  String dto) {

        StudentDto dtos = gson.fromJson(dto, StudentDto.class);

        StudentDao dao = new StudentDao();
        dao.setBith_day(dtos.getBith_day());
        dao.setName(dtos.getName());
        dao.setSurname(dtos.getSurname());
        dao.setTown(dtos.getTown());
        studentService.save(dao);

        return "students";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public String delete(@RequestParam int id) {
        studentService.delete(id);

        return "students";
    }
}
