package com.WebServer.controller;

import com.WebServer.Service.StudentService;
import com.WebServer.dao.StudentDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;


@Controller
public class HelloController {

    @Autowired
    StudentService studentService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String hello() {

        return "index";
    }

    @RequestMapping(value = "/view", method = RequestMethod.GET)
    @ResponseBody
    public String demo1() {
        return studentService.findall().toString();
    }

    @RequestMapping(value = "newrow", method = RequestMethod.GET)
    @ResponseBody
    public String newrow(){
        try {
            studentService.deleteall();
            StudentDao dao = new StudentDao();
            dao.setTown("Oskol");
            dao.setSurname("Orlov");
            dao.setName("Artem");
            dao.setBith_day(new Date());
            dao.setDate_created(new Date());
            studentService.save(dao);
            StudentDao dao2 = new StudentDao();
            dao2.setTown("Oskol2");
            dao2.setSurname("Orlov2");
            dao2.setName("Artem2");
            dao2.setBith_day(new Date());
            dao2.setDate_created(new Date());
            studentService.save(dao2);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return "Not Ok";
        }
        return "Ok";
    }
}
