package com.WebServer.dao;

import com.WebServer.Service.StudentService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity
public class UserDao {

    public UserDao() {}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private int childId;
    private String character;
    private String login;
    private String password;
    private String role;
    private boolean blocked;
}
