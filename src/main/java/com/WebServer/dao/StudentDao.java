package com.WebServer.dao;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
public class StudentDao {

    public StudentDao() {}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private String surname;
    private String town;
    private Date bith_day;
    private Date date_created;
}
