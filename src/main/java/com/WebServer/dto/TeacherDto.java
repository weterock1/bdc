package com.WebServer.dto;

import lombok.Data;

import java.util.Date;

@Data
public class TeacherDto {

    private String name;
    private String surname;
    private String town;
    private Date bith_day;
    private String descr;
}
