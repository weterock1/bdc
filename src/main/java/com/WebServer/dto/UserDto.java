package com.WebServer.dto;

import lombok.Data;

@Data
public class UserDto {

    private String character;
    private String login;
    private String password;
    private String role;
}
