package com.WebServer.Service;

import com.WebServer.Reprository.UserRepository;
import com.WebServer.dao.UserDao;
import com.WebServer.dto.UserDto;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service("userDetailsService")
@Data
@Transactional
public class UserService implements UserDetailsService{

    @Autowired
    UserRepository userRepository;

    public void deleteall() {
        for (UserDao userdao : userRepository.findAll()) {
            userRepository.delete(userdao.getId());
        }
    }

    public List<UserDto> findall() {
        List<UserDto> userDtos = new ArrayList<>();

        for (UserDao userDao : userRepository.findAll()) {
            UserDto dto = new UserDto();
            dto.setCharacter(userDao.getCharacter());
            dto.setLogin(userDao.getLogin());
            dto.setPassword(userDao.getPassword());
            dto.setRole(userDao.getRole());
            userDtos.add(dto);
        }
        return userDtos;
    }

    public void save(UserDao dao) {
        userRepository.save(dao);
    }

    public void delete(int id) {
        userRepository.delete(id);
    }

    public UserDao findbyLogin(String name) {
        for (UserDao dao : userRepository.findAll()) {
         if (dao.getLogin().equals(name)) {
             return dao;
         }
            throw new UsernameNotFoundException("User with name " + name + " is not exists");
        }
        return null;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        UserDao dao = findbyLogin(s);
        List<GrantedAuthority> authorities = buildUserAuthority(dao.getRole());
        return buildUserForAuthentication(dao,authorities);
    }

    private User buildUserForAuthentication(UserDao user, List<GrantedAuthority> authorities) {
        return new User(user.getLogin(), user.getPassword(),
                !user.isBlocked(), true, true, true, authorities);
    }

    private List<GrantedAuthority> buildUserAuthority(String role) {
        Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();
        setAuths.add(new SimpleGrantedAuthority(role));
        return new ArrayList<GrantedAuthority>(setAuths);
    }

}
