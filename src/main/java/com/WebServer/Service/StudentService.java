package com.WebServer.Service;

import com.WebServer.Reprository.StudentRepository;
import com.WebServer.dao.StudentDao;
import com.WebServer.dto.StudentDto;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Data
@Transactional
public class StudentService {

    @Autowired
    StudentRepository studentRepository;

    public void deleteall() {
        for (StudentDao studentDao : studentRepository.findAll()) {
            studentRepository.delete(studentDao.getId());
        }
    }

    public List<StudentDto> findall() {
        List<StudentDto> studentDTO = new ArrayList<>();

        for (StudentDao studentDao : studentRepository.findAll()) {
            StudentDto dto = new StudentDto();
            dto.setBith_day(studentDao.getBith_day());
            dto.setName(studentDao.getName());
            dto.setSurname(studentDao.getSurname());
            dto.setTown(studentDao.getTown());
            studentDTO.add(dto);
        }
        return studentDTO;
    }

    public void save(StudentDao dao) {
        studentRepository.save(dao);
    }

    public void delete(int id) {
        studentRepository.delete(id);
    }
}
