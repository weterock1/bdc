package com.WebServer.Service;

import com.WebServer.Reprository.TeacherRepository;
import com.WebServer.dao.StudentDao;
import com.WebServer.dao.TeacherDao;
import com.WebServer.dto.StudentDto;
import com.WebServer.dto.TeacherDto;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Data
@Transactional
public class TeacherService {

    @Autowired
    TeacherRepository teacherRepository;

    public void deleteall() {
        for (TeacherDao teacherDao : teacherRepository.findAll()) {
            teacherRepository.delete(teacherDao.getId());
        }
    }

    public List<TeacherDto> findall() {
        List<TeacherDto> teacherDtos = new ArrayList<>();

        for (TeacherDao dao : teacherRepository.findAll()) {
            TeacherDto dto = new TeacherDto();
            dto.setBith_day(dao.getBith_day());
            dto.setName(dao.getName());
            dto.setSurname(dao.getSurname());
            dto.setTown(dao.getTown());
            dto.setDescr(dao.getDescr());
            teacherDtos.add(dto);
        }
        return teacherDtos;
    }

    public void save(TeacherDao dao) {
        teacherRepository.save(dao);
    }

    public void delete(int id) {
        teacherRepository.delete(id);
    }
}
