$(document).ready(function () {
    $('#buttonDemo').click(function () {
        $.ajax({
            type: "GET",
            url: 'newrow',
            success: result => {
                alert(result);
            },
            error: function () {
                alert("Что-то пошло не так");
            }
        });
    });

    $('#buttonDemo2').click(function () {
        $.ajax({
            type: "GET",
            url: 'view',
            success: result => {
            alert(result);
            }
        });
    });

    $('#buttonDemo3').click(function () {
        $.ajax({
            type: "GET",
            url: 'student/home',
            success: result => {
                var uri = "student/";
                document.location.href = uri;
            }
        });
    });
});