<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Cache-Control" content="no-cache">
    <meta http-equiv="Expires" content="Sat, 01 Dec 2001">

    <title> Layout | HOME</title>
    <link href="../boot/css/bootstrap.min.css" rel = "stylesheet">


</head>

<body>

    <div role="navigation">
        <div class="navbar navbar-inverse">
            <a href="#" class="navbar-brand">StudentPage</a>
        </div>
    </div>

    <script src="../boot/js/jquery-3.1.1.min.js"></script>
    <script src="../boot/js/bootstrap.min.js"></script>
</body>
</html>