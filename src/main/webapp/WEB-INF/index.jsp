<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html">
    <script type="text/javascript" src = "../boot/js/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="../boot/js/glm-ajax.js"></script>
    <script type="text/javascript" src="../boot/ownjs/index.js"></script>
</head>
<body>
<h2>
    <fieldset>
        <legend>
            Demo1
        </legend>
        <input type = "button" value="Demo 1" id="buttonDemo">
        <input type="button" value="Demo 2" id="buttonDemo2">
        <input type="button" value="На страничку студента" id="buttonDemo3">
        <span id="result"></span>
    </fieldset>
</h2>
</body>
</html>